﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Net;
using System.Text.RegularExpressions;

namespace PuushManager
{
    public class PuushFile
    {
        public string Name;
        public string ID;
        public string Hits;
        public string Size;

        public PuushFile(string title, string url)
        {            
            Match match = Regex.Match(title, @"(?<name>.*?) \((?<hits>\d*\+? hits?)\)");
            Name = match.Groups["name"].Value;
            Hits = match.Groups["hits"].Value;
            ID = url.Replace("http://puu.sh/","");
        }

        public PuushFile(string title, string url, string size) : this(title, url)
        {
            Size = size;
        }

        public override string ToString()
        {
            return Name;
        }
    }

    public static class PuushAPI
    {
        public static bool ValidAPIKey(string key)
        {
            using (CookieAwareWebClient wc = new CookieAwareWebClient())
            {
                wc.Headers[HttpRequestHeader.ContentType] = "application/x-www-form-urlencoded";

                string htmlResult = wc.UploadString("http://puush.me/api/auth", "k="+key);

                return !string.IsNullOrEmpty(htmlResult) && htmlResult != "-1";
            }            
        }

        public static string GetAPIKey(string username, string password)
        {
            using (CookieAwareWebClient wc = new CookieAwareWebClient())
            {
                wc.Headers[HttpRequestHeader.ContentType] = "application/x-www-form-urlencoded";

                string parameters = "e=" + username + "&p=" + password;
                string htmlResult = wc.UploadString("http://puush.me/api/auth", Uri.EscapeUriString(parameters));

                if (string.IsNullOrEmpty(htmlResult) || htmlResult == "-1") return null;

                string[] result = htmlResult.Split(',');
                return result[1];
            }
        }

        public static List<PuushFile> ScrapePage(int page, string apikey)
        {
            using (CookieAwareWebClient wc = new CookieAwareWebClient())
            {
                wc.Headers[HttpRequestHeader.ContentType] = "application/x-www-form-urlencoded";                

                wc.UploadString("http://puush.me/login/go?k=" + apikey, ""); // Log in
                string htmlResult = wc.UploadString("http://puush.me/account?list&page="+page, ""); // Get page

                List<PuushFile> result = new List<PuushFile>();

                try
                {
                    //MatchCollection matches = Regex.Matches(htmlResult, @"<a\sonclick=""(?<onclick>.*?)""\shref=""(?<url>.*?)""\starget=""(?<target>.*?)""\stitle=""(?<title>.*?)"">(?<text>.*?)</a>", RegexOptions.IgnoreCase | RegexOptions.Compiled);
                    //if (matches.Count == 0)
                    //{
                    MatchCollection matches = Regex.Matches(htmlResult, @"<a href=""(?<url>.*?)"" target=""(?<target>.*?)"" onclick=""(?<onclick>.*?)"">(?<text>.*?)</a></td><td>(?<size>.*?)</td><td>(?<hits>.*?)</td>", RegexOptions.IgnoreCase | RegexOptions.Compiled);
                    result.AddRange(from Match m in matches select new PuushFile(m.Groups["text"].Value + " (" + m.Groups["hits"].Value + ")", m.Groups["url"].Value, m.Groups["size"].Value));
                    //}
                    //else
                    //{
                    //    result.AddRange(from Match m in matches select new PuushFile(m.Groups["title"].Value, m.Groups["url"].Value));
                    //}
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }

                return result;
            }
        }

        public static int GetPages(string apikey)
        {
            using (CookieAwareWebClient wc = new CookieAwareWebClient())
            {
                wc.Headers[HttpRequestHeader.ContentType] = "application/x-www-form-urlencoded";

                wc.UploadString("http://puush.me/login/go?k=" + apikey, ""); // Log in
                string htmlResult = wc.UploadString("http://puush.me/account?page=1", ""); // Get page

                List<int> results = new List<int>();
                
                try
                {
                    MatchCollection matches = Regex.Matches(htmlResult, @"<a\shref=""\?page=\d+"">(?<text>.*?)</a>", RegexOptions.IgnoreCase | RegexOptions.Compiled);
                    results.AddRange(from Match m in matches select int.Parse(m.Groups["text"].Value));
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }

                return results.Count == 0 ? 0 : results.Max();
            }            
        }
    }
}
