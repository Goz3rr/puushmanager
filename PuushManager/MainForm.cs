﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows.Forms;

namespace PuushManager
{
    public partial class MainForm : Form
    {
        private String _apiKey;
        private bool _loaded;

        public MainForm()
        {
            InitializeComponent();

            statusStrip.Padding = new Padding(statusStrip.Padding.Left, statusStrip.Padding.Top, statusStrip.Padding.Left, statusStrip.Padding.Bottom);
        }

        private void MainForm_Shown(object sender, EventArgs e)
        {
            bool result = false;
            while (string.IsNullOrEmpty(_apiKey) || !PuushAPI.ValidAPIKey(_apiKey))
            {
                result = ShowLoginDialog();
                if (!result)
                {
                    break;
                }
            }

            if (!result)
            {
                Application.Exit();
            }
            else
            {
                lblStatus.Text = "Retrieving pages";
                
                BackgroundWorker worker = new BackgroundWorker();
                worker.WorkerReportsProgress = true;
                worker.DoWork += delegate(object o, DoWorkEventArgs args)
                    {                    
                        int pages = PuushAPI.GetPages(_apiKey);
                        ((BackgroundWorker)o).ReportProgress(pages);

                        List<PuushFile> threadresult = new List<PuushFile>();

                        for (int i = 1; i <= pages; i++)
                        {                            
                            ((BackgroundWorker)o).ReportProgress(i);
                            List<PuushFile> files = PuushAPI.ScrapePage(i, _apiKey);

                            threadresult.AddRange(files);
                        }

                        args.Result = threadresult;
                    };

                bool receivedPages = false;
                int totalpages = 0;                
                worker.ProgressChanged += delegate(object o, ProgressChangedEventArgs args)
                    {
                        if (!receivedPages)
                        {
                            receivedPages = true;
                            totalpages = args.ProgressPercentage;
                            lblStatus.Text = "Received " + totalpages + " pages";
                        }
                        else
                        {
                            progressBar.Value = (int) (args.ProgressPercentage * (100f / totalpages));
                            lblStatus.Text = "Loading page " + args.ProgressPercentage + " of " + totalpages;
                        }
                    };

                worker.RunWorkerCompleted += delegate(object o, RunWorkerCompletedEventArgs args)
                    {
                        progressBar.Value = 100;
                        _loaded = true;

                        lblStatus.Text = "Ready";

                        if (args.Cancelled || args.Error != null) return;

                        foreach (PuushFile file in (List<PuushFile>)args.Result)
                        {
                            lstImages.Items.Add(file);
                        }
                    };

                worker.RunWorkerAsync();
            }
        }

        private bool ShowLoginDialog()
        {
            using (LoginDialog dialog = new LoginDialog())
            {
                if (dialog.ShowDialog() == DialogResult.OK)
                {
                    _apiKey = dialog.APIKey;
                    return true;
                }
                return false;
            }
        }

        private static bool ShowDownloadDialog(IList<PuushFile> filesToDownload, string path)
        {
            using (DownloadDialog dialog = new DownloadDialog(filesToDownload, path))
            {
                return dialog.ShowDialog() == DialogResult.OK;
            }
        }

        private void btnDownloadAll_Click(object sender, EventArgs e)
        {
            if (!_loaded) return;

            FolderBrowserDialog folderBrowserDialog = new FolderBrowserDialog();
            if (folderBrowserDialog.ShowDialog() == DialogResult.OK)
            {
                List<PuushFile> filelist = lstImages.Items.Cast<PuushFile>().ToList();

                ShowDownloadDialog(filelist, folderBrowserDialog.SelectedPath);
            }
        }

        private void lstImages_SelectedIndexChanged(object sender, EventArgs e)
        {
            PuushFile file = (PuushFile)lstImages.Items[lstImages.SelectedIndex];

            lblViews.Text = file.Hits;
            lblSize.Text = file.Size;
            img.ImageLocation = "http://puu.sh/" + file.ID;
        }
    }
}
