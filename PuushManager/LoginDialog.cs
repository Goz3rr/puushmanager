﻿using System;
using System.Windows.Forms;

namespace PuushManager
{
    public partial class LoginDialog : Form
    {
        public string APIKey;

        public LoginDialog()
        {
            InitializeComponent();

            btn_ok.DialogResult = DialogResult.OK;
        }

        private void btn_ok_Click(object sender, EventArgs e)
        {
            APIKey = txtAPIKey.TextLength == 0 ? PuushAPI.GetAPIKey(txtUsername.Text, txtPassword.Text) : txtAPIKey.Text;
        }
    }
}
