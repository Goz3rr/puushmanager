﻿namespace PuushManager
{
    partial class DownloadDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblDL = new System.Windows.Forms.Label();
            this.DLProgress = new System.Windows.Forms.ProgressBar();
            this.btnCancelDL = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lblDL
            // 
            this.lblDL.AutoSize = true;
            this.lblDL.Location = new System.Drawing.Point(9, 8);
            this.lblDL.Name = "lblDL";
            this.lblDL.Size = new System.Drawing.Size(115, 13);
            this.lblDL.TabIndex = 0;
            this.lblDL.Text = "Downloading file 0 of 0";
            // 
            // DLProgress
            // 
            this.DLProgress.Location = new System.Drawing.Point(12, 37);
            this.DLProgress.Name = "DLProgress";
            this.DLProgress.Size = new System.Drawing.Size(420, 23);
            this.DLProgress.TabIndex = 1;
            // 
            // btnCancelDL
            // 
            this.btnCancelDL.Location = new System.Drawing.Point(357, 8);
            this.btnCancelDL.Name = "btnCancelDL";
            this.btnCancelDL.Size = new System.Drawing.Size(75, 23);
            this.btnCancelDL.TabIndex = 2;
            this.btnCancelDL.Text = "Cancel";
            this.btnCancelDL.UseVisualStyleBackColor = true;
            this.btnCancelDL.Click += new System.EventHandler(this.btnCancelDL_Click);
            // 
            // DownloadDialog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(444, 72);
            this.Controls.Add(this.btnCancelDL);
            this.Controls.Add(this.DLProgress);
            this.Controls.Add(this.lblDL);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "DownloadDialog";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Downloading files";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblDL;
        private System.Windows.Forms.ProgressBar DLProgress;
        private System.Windows.Forms.Button btnCancelDL;
    }
}