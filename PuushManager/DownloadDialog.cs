﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net;
using System.Windows.Forms;

namespace PuushManager
{
    public partial class DownloadDialog : Form
    {
        private readonly IList<PuushFile> _downloadingQueue;
        private readonly Stopwatch _sw = new Stopwatch();

        public DownloadDialog(IList<PuushFile> downloadFiles, string downloadLocation)
        {            
            InitializeComponent();
            btnCancelDL.DialogResult = DialogResult.Cancel;

            _downloadingQueue = downloadFiles;
            int count = 0;

            _sw.Start();
            for (int i = 0; i < downloadFiles.Count; i++)
            {
                try
                {
                    using (WebClient wc = new WebClient())
                    {
                        PuushFile file = downloadFiles[i];
                        wc.DownloadFileAsync(new Uri("http://puu.sh/" + file.ID), downloadLocation + "\\" + file.Name);
                        wc.DownloadFileCompleted += (o, args) => UpdateStatus(++count, downloadFiles.Count);
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }
            }

            /*
            BackgroundWorker worker = new BackgroundWorker();
            worker.WorkerReportsProgress = true;

            worker.DoWork += delegate(object o, DoWorkEventArgs args)
                {
                    using (WebClient wc = new WebClient())
                    {
                        for (int i = 0; i < downloadFiles.Count; i++)
                        {                            
                            try
                            {
                                PuushFile file = downloadFiles[i];
                                //Uri file = new Uri("http://puu.sh/" + downloadFiles[i].ID);
                                //Stream stream = wc.OpenRead(file);
                                //string filename = new ContentDisposition(wc.ResponseHeaders["content-disposition"]).FileName;
                                int i1 = i;
                                wc.DownloadFileAsync(new Uri("http://puu.sh/" + file.ID), downloadLocation + "\\" + file.Name);                                
                                wc.DownloadFileCompleted += delegate(object o2, AsyncCompletedEventArgs args2)
                                    {
                                        ((BackgroundWorker)o).ReportProgress(i1 + 1);
                                    };
                                //if (stream != null) stream.Close();
                            }
                            catch (Exception e)
                            {
                                Console.WriteLine(e.Message);
                            }                            
                        }
                    }
                };

            worker.ProgressChanged += delegate(object o, ProgressChangedEventArgs args)
                {
                    UpdateStatus(args.ProgressPercentage, downloadFiles.Count);
                };

            worker.RunWorkerCompleted += delegate(object o, RunWorkerCompletedEventArgs args)
                {
                    lblDL.Text += " Done!";

                    btnCancelDL.DialogResult = DialogResult.OK;
                    btnCancelDL.Text = "Close";
                };

            worker.RunWorkerAsync();
            */
            /*
            int count = 0;

            for (int i = 0; i < downloadFiles.Count; i++)
            {
                string file = downloadFiles[i];

                using (WebClient wc = new WebClient())
                {
                    wc.OpenReadAsync(new Uri("http://puu.sh/" + file));
                    wc.OpenReadCompleted += delegate(object sender, OpenReadCompletedEventArgs e)
                        {                            
                            string headerContentDisposition = wc.ResponseHeaders["content-disposition"];
                            string filename = new ContentDisposition(headerContentDisposition).FileName;

                            //Console.WriteLine("http://puu.sh/" + file + "  " + downloadLocation + "\\" + filename);
                            wc.DownloadFileAsync(new Uri("http://puu.sh/" + file), downloadLocation + "\\" + filename);
                            wc.DownloadFileCompleted += delegate(object sender1, AsyncCompletedEventArgs e1)
                                {
                                    count++;
                                    UpdateStatus(count, downloadFiles.Count);
                                };

                            e.Result.Close();
                        };
                }
            }   
            */
        }

        private void UpdateStatus(int current, int total)
        {
            if (current >= total)
            {
                _sw.Stop();
                lblDL.Text = string.Format("Done! Completed in {0} seconds", _sw.Elapsed);
                DLProgress.Value = 100;

                btnCancelDL.DialogResult = DialogResult.OK;
                btnCancelDL.Text = "Close";
            }
            else
            {
                lblDL.Text = string.Format("Downloading file {0} of {1}..\n{2}", current, total, _downloadingQueue[current].Name);
                DLProgress.Value = (int) ((float) current/total*100);
            }
        }

        private void btnCancelDL_Click(object sender, EventArgs e)
        {
            
        }
    }
}
